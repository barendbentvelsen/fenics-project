# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository contains files for the project "Modal analysis and structural stability using FEniCS" which was an assignment for the 2017 Course on Mathematical Modelling for FEniCS.
* version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* The repository contains Jupyter notebooks in which modal solvers are written and some examples are documented.
* The Jupyter Notebooks runs on fenics 2016.2.0
* After cloning the repository, you can open the notebooks using Jupyter and run the scripts if you have FEniCs installed. 

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* This project is made by Ana Budisa and Barend Bentvelsen
