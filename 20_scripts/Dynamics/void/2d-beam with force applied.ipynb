{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Dynamic analysis of 2D-beam"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this notebook we have a cantilever beam which is clamped at the left-hand side and a force is applied on the right hand side.\n",
    "\n",
    "The analysis is based on linear elasticity and a Newmark time-integration to plot the displacement over time.\n",
    "\n",
    "This is an example of a dynamic structure and is a benchmark to modal analysis."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "from dolfin import *\n",
    "import mshr\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "# Form compiler options\n",
    "parameters[\"form_compiler\"][\"cpp_optimize\"] = True\n",
    "parameters[\"form_compiler\"][\"optimize\"] = True\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The example is a domain with length of 1 meter and a width of 2.5 centimeters. We take material properties of steel and do not take damping into account by setting it to zero.\n",
    "\n",
    "The force applied is a lineaic pressure of 100 kPa per meter at the end of the beam. This gives 2,5 kN force per meter if we take the height of 2,5 cm into account.\n",
    "\n",
    "Gravity is taken into account but it can be neglected under the current material properties and applied load. However, for future applications of modal analysis it is easy to have a body force included as well.\n",
    "\n",
    "The force and inertia are per meter as the width of the beam is not defined in 2D (force, density)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Domain parameters\n",
    "ll = 1.0\n",
    "ww = 0.025\n",
    "\n",
    "# Mesh density\n",
    "dens = 30\n",
    "\n",
    "# Elasticity parameters\n",
    "E, nu = 2.1e9, 0.3\n",
    "mu, lmbda = Constant(E/(2*(1 + nu))), Constant(E*nu/((1 + nu)*(1 - 2*nu)))\n",
    "\n",
    "# Mass density and viscous damping coefficient\n",
    "rho, eta = 7.8e4, 0.0\n",
    "\n",
    "# applied force\n",
    "pressure = -5e4\n",
    "grav = -9.81"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Creating a mesh and function space with the relevant functions:\n",
    "\n",
    "There are a number of functions defined on the vectorspace $\\mathcal{V}$: the displacement, velocity and acceleration. \n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Create mesh\n",
    "P0 = Point(0., 0.)\n",
    "P1 = Point(ll, ww)\n",
    "domain = mshr.Rectangle(P0, P1)\n",
    "mesh = mshr.generate_mesh(domain, dens)\n",
    "\n",
    "# Create function space\n",
    "V = VectorFunctionSpace(mesh, \"Lagrange\", 1)\n",
    "\n",
    "# Create test and trial functions, and source term\n",
    "u1, w = TrialFunction(V), TestFunction(V)\n",
    "\n",
    "# Fields from previous step\n",
    "u0, v0, a0 = Function(V), Function(V), Function(V)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Time stepping parameters are set so that we look at the first natural period with a small time step. These settings have to be set here to determine the first step of the velocity and acceleration."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "period0 = 0.71337826\n",
    "# Time stepping parameters\n",
    "beta, gamma = 0.25, 0.5\n",
    "dt = 0.005*period0\n",
    "t, T = 0.0, period0*2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Calculating velocity and acceleration at the first time step using $u0$, $v0$ and $a0$ defined in the previous block effectively setting initial position and velocity to zero"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Velocity and acceleration at first time step\n",
    "v1 = (gamma/(beta*dt))*(u1 - u0) - (gamma/beta - 1.0)*v0 - dt*(gamma/(2.0*beta) - 1.0) * a0\n",
    "a1 = (1.0/(beta*dt**2))*(u1 - u0 - dt*v0) - (1.0/(2.0*beta) - 1.0)*a0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Marking the boundaries an applying boundary conditions. The beam is clamped on the left, therefore Dirichlet boundary conditions are applied to set the displacement to zero there."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Mark boundary subdomians\n",
    "class BoundaryLeft(SubDomain):\n",
    "    def inside(self, x, on_boundary):\n",
    "        return on_boundary and (near(x[0], 0.))\n",
    "    \n",
    "class BoundaryRight(SubDomain):\n",
    "    def inside(self, x, on_boundary):\n",
    "        return on_boundary and (near(x[0], ll))\n",
    "    \n",
    "class BoundaryTop(SubDomain):\n",
    "    def inside(self, x, on_boundary):\n",
    "        return on_boundary and (near(ww, x[1]))\n",
    "    \n",
    "class BoundaryBottom(SubDomain):\n",
    "    def inside(self, x, on_boundary):\n",
    "        return on_boundary and (near(0, x[1]))\n",
    "\n",
    "# set boundary markers\n",
    "boundary_markers = FacetFunction('size_t', mesh)\n",
    "left = BoundaryLeft()\n",
    "right = BoundaryRight()\n",
    "top = BoundaryTop()\n",
    "bottom = BoundaryBottom()\n",
    "left.mark(boundary_markers, 0)\n",
    "top.mark(boundary_markers, 1)\n",
    "right.mark(boundary_markers, 2)\n",
    "bottom.mark(boundary_markers, 3)\n",
    "\n",
    "# Define measure ds to apply markers\n",
    "ds = Measure('ds', domain=mesh, subdomain_data=boundary_markers)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Defining loads and taking inertia into account for the governing balance equation\n",
    "\n",
    "The material is modeled as homogenous and isotropic so the stress tensor is given simply by:\n",
    "$$\n",
    "\\boldsymbol{\\sigma} = \\mu\\boldsymbol{\\epsilon} + \\lambda \\text{tr}(\\boldsymbol{\\epsilon})\\mathbf{I}\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Define loads\n",
    "b = Constant((0.0, grav))\n",
    "tt = Constant((0.0, pressure))\n",
    "\n",
    "# Define boundary conditions\n",
    "bc = DirichletBC(V, Constant((0.0, 0.0)), left)\n",
    "\n",
    "# Stress function\n",
    "def sigma(u, v):\n",
    "    return 2.0*mu*sym(grad(u)) + (lmbda*tr(grad(u)) + eta*tr(grad(v)))*Identity(len(u))\n",
    "\n",
    "# Governing balance equation\n",
    "F = (rho*dot(a1, w) + inner(sigma(u1, v1), sym(grad(w))))*dx - dot(b, w)*dx - dot(tt, w)*ds(2)\n",
    "\n",
    "# Extract bilinear and linear forms from F\n",
    "a, L = lhs(F), rhs(F)\n",
    "\n",
    "u = Function(V)\n",
    "A, b = assemble_system(a, L, bc)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Writing a Newmark-integration scheme to update the problem based on average constant acceleration:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Define update\n",
    "def update(u, u0, v0, a0, beta, gamma, dt):\n",
    "    # get vectors (reference)\n",
    "    u_vec, u0_vec = u.vector(), u0.vector()\n",
    "    v0_vec, a0_vec = v0.vector(), a0.vector()\n",
    "    \n",
    "    # update acceleration and velocity\n",
    "    a_vec = (1.0 / (2.0*beta))*((u_vec - u0_vec - v0_vec*dt)/(0.5*dt*dt) - (1.0 -2.0*beta)*a0_vec)\n",
    "    # v = dt * ((1-gamma)*a0 + gamma*a) +v0\n",
    "    v_vec = dt*((1.0-gamma)*a0_vec + gamma*a_vec) + v0_vec\n",
    "    \n",
    "    # Update (t(n) <-- t(n+1))\n",
    "    v0.vector()[:], a0.vector()[:] = v_vec, a_vec\n",
    "    u0.vector()[:] = u.vector()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Integrating the problem over time and applying the load at each time step:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "file = File(\"beam_direct/displacement.pvd\")\n",
    "file << (u, float(t))\n",
    "while t <= T:\n",
    "    t+=dt\n",
    "    b = assemble(L)\n",
    "    bc.apply(b)\n",
    "    solve(A, u.vector(), b)\n",
    "    update(u, u0, v0, a0, beta, gamma, dt)\n",
    "    file << (u, float(t))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plot the displacement at the last time step:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAh8AAADBCAYAAABv9tKnAAAABHNCSVQICAgIfAhkiAAAAAlwSFlz\nAAAPYQAAD2EBqD+naQAAIABJREFUeJzt3X2cHFWd7/HPNxNAghKQGBBBkEUxuIokomT3rjytZlmf\n7q4KDoJPV68PqBhfCKtXhYu6XFwFwZXXckERFhiX9QEfECMBV3chwCWDkZUgLIYngUB4GISEPMyc\n+8epzlRqqrq7Jl3V3TPf9+vVr54+59Spc850d/361JNCCJiZmZnVZUa3G2BmZmbTi4MPMzMzq5WD\nDzMzM6uVgw8zMzOrlYMPMzMzq5WDDzMzM6uVgw8zMzOrlYMPMzMzq5WDDzMzM6uVgw8zMzOrVS3B\nh6TjJa2StE7SDZIOalJ2f0nfTcqPSfr41tZpZmZmvaPy4EPS0cBXgVOAA4EVwBJJcwoWmQXcBZwM\nPNihOs3MzKxHqOoby0m6AbgxhHBC8lrAfcA5IYQvt1h2FXBWCOGcTtVpZmZm3VXpzIekbYAFwDWN\ntBCjnaXAwl6p08zMzOozs+L65wADwOpM+mpgv7rqlLQLsAi4G3hmkus1MzObjp4F7A0sCSE82okK\nqw4+igjo9P6eZnUuAi7t8PrMzMymk3cCl3WioqqDjzXAKLBrJn0uE2cuqqzzboBLLrmEefPmTXK1\n08/ixYs566yzut2MvuNxK89jNjket/I8ZuWtXLmSY489FpJtaSdUGnyEEDZKWg4cAfwINh8cegRw\nTrNlO1znMwDz5s1j/vz5k1nttDR79myP1yR43MrzmE2Ox608j9lW6dhhC3XsdjkTuCgJGG4CFhNP\np/02gKSLgftDCJ9JXm8D7E/cjbIt8AJJBwBPhRDuaqdOMzMz612VBx8hhMuT62+cRtxV8mtgUQjh\nkaTIHsCm1CK7A7cwfvzGicnjl8DhbdZpZmZmPaqWA05DCOcC5xbkHZ55fQ9tnALcrE4zMzPrXb63\nixUaHBzsdhP6ksetPI/Z5HjcyvOY9YbKr3DaCyTNB5YvX77cBxqZmZmVMDw8zIIFCwAWhBCGO1Gn\nZz7MzMysVg4+zMzMrFYOPszMzKxWDj7MzMysVg4+zMzMrFYOPszMzKxWtQQfko6XtErSOkk3SDqo\nRfm3S1qZlF8h6chM/oWSxjKPn1bbCzMzM+uEyoMPSUcDXwVOAQ4EVgBLksuj55VfSLxl7/nAK4Er\ngCsk7Z8pehXx0uq7JQ9fOcbMzKwP1DHzsRg4L4RwcQjhduBDwFrgfQXlTwCuCiGcGUL4XQjhFGAY\n+Gim3PoQwiMhhIeTx0hlPTAzM7OOqTT4SO5QuwC4ppEW4iVVlwILCxZbmOSnLckpf6ik1ZJul3Su\npOd2qNlmZmZWoapnPuYAA8DqTPpq4q6SPLu1Uf4q4F3Eu9yeBBwC/FSStrbBZmZmVq1a7mqbQ0CZ\nm8psUT6EcHkq77eSbgXuAg4FftGJBpqZmVk1qg4+1gCjxAND0+YycXaj4aGS5QkhrJK0BtiXJsHH\n4sWLmT179hZpg4ODvsuhmZkZMDQ0xNDQ0BZpIyOdP6Sy8rvaSroBuDGEcELyWsC9wDkhhH/IKf8d\nYPsQwltSadcBK0IIHylYxx7APcBbQgg/ycn3XW3NzMwmoYq72tax2+VM4CJJy4GbiGe/zAK+DSDp\nYuD+EMJnkvJnA7+U9EngSuIptAuADyTldyCetvs94izJvsAZwB3EA1PNzMysh1UefIQQLk+u6XEa\ncXfKr4FFIYRHkiJ7AJtS5ZdJGgS+lDzuJM5o3JYUGQVeQTzgdCfgAWLQ8fkQwsaq+2NmZmZbp5YD\nTkMI5wLnFuQdnpP2PeLMRl75Z4C/6mgDzczMrDa+t4uZmZnVysGHmZmZ1crBh5mZmdWqWxcZM+uI\nvbl989878wTbs25CmV0LLhEzt/jSMczh0dz0XXLSd2FNYT0780Ru+k4F6c/hjxPStmdtbtnt2FC4\n3m0L8gbGj+3ebLTJ18AoA7npG9g2N30tsyakPcFOhfWvZVZhXWvIvfckD/D8CWkPsnvhOu5jz9z0\nVeydm34rTW+6bWYdMK2CjwULzoMJX1JFQ7BNk5qek5O2fcEyOzapo2gdcwvSs9dea1Y/sHNBet6F\n7Rvf89tl0vO/t9m8PXl2Tt4eTep/ViZ97/zqZ+zydGzO9hM3onN3LA4ayljHrMINe6esZzu2Y31l\n5as2wGhhANJu+W3ZUBhg9JoHx3ZnuyeenJC+4dcFn7O7oSCOhGU5aXclz9l49b6J6xxfAZATlOZ/\nD82LT8p8txxRUP0rk+e8z/kbi68BtXCff5uQdliTi0v/5YTbdSXLPJIzSDcTz2HM+n5B5TfD3Q/n\nZ11UsMgpFV/fylrzbpeOKnumb9nyE3/VN/VMyerz5H3nbY1sm1rUv35duY3WOrYvWX7iL/Vm8jai\nGyZEbJ3VbGaiE/JmQ7bMHy1ZX7nyRbM0RWaV/ByUDTCfs1POmzJ/EiYqntgpocmPiE7oTLzes/Yu\n+r1mPcvBR4sv3q3X6UuPVByAlP3R/VSbac2UDHDKbuzzdgVsjbK/4teXbG/Z8p3QKgCZWL7aAKOV\nbADSKsDoSADSTNkApFkwkytvlqOJspc8uq9c8YdzZmdX587Mts7LlbcXrXjPmvUhBx8d14nZj5IB\nRlllA5JW38Nlg42S6+/G7Mf6kgFG2YCobP1Vz3600o3Zj7K7n8oGGGVnUEoHDM8rWb7l7Ec2AGnR\n/mwA0mr2IxuA3N/8JuF5AUgzHQlAmvDsR39x8AGUn/1oVT4bULQKSHps90ved37Z3S8dnv3IBiCt\nNvbZAKTV7Ecndr8004nZj17b/dIqwMjmt5r96IvdL810ZPaj7O6Xkt8FZXe/tAhAJlZfLsBY87y8\ng8aaKBmQ7FWuuNWoluBD0vGSVklaJ+kGSU0PJ5f0dkkrk/IrJB2ZU+Y0SQ9IWivpakn7bl0rOx2A\nZFUcYLSSDUBaBSSd2P3STN76e2z3S6dnP7IBRav6vful+7MfEwKQVrMf2QCk1exHH+5++ZN9btv8\ndz/MfjgA6U2VBx+Sjga+SrwZ3IHACmBJcr+XvPILgcuA84nHYl8BXCFp/1SZk4GPAh8EXg08ndTZ\nQ4fUV737pQPBSad3v2S1Ckj6YPdLM504+NS7X7bUidmPZgFIXl7/7X7J6q/Zj2x+y9mPbADSIiDx\n7pf+UMfMx2LgvBDCxSGE24EPAWuB9xWUPwG4KoRwZgjhdyGEU4BhYrCRLvOFEMKPQwj/SbzJ3O7A\nf9+6pnr3yxamwMGnW7v7pVVw4INPOx9gZPNbzX5Mzd0vzXR/9iOt7MGnebz7ZfqpNPiQtA2wALim\nkRZCCMBSYGHBYguT/LQljfKS9iFeqSJd55PAjU3qrNAU3/2S1emDT0vKm/2Y6rtfsnpx9mMyAUiZ\n/0O3d79M0PXZj5IHn2b54FPrsqpnPuYAA0x8q68m/1JXJOnNyu8KhJJ1llD1qbetdPrU3DZ0e/fL\n1tbfYWV3v+Tp990vZWc/JmNWxRd4a6bqi8vlzn5490tTnv2YXrr1E0rEAKKT5VuWee1rb2f27Ae3\nSBscHGRwcLBEU6weOzTJe1FtrbBppujn2CsmUVfRjuVSJrNikLrwI6ak1ezKy7m1svr3ngtkrny6\nF3BPZWucGoaGhhgaGtoibWRkpOPrqTr4WAOMMvG64HMpjrsfalH+IWKgsWumjrnALc0ac9ZZZzF/\n/vzWrTYz62MhNLs9xNYQ8LKCvKL0Ep4HTDi3MVGU3sR7tqIp01XeD/Lh4WEWLFjQ0fVUutslhLAR\nWE7qzgKSlLy+vmCxZUy8E8HrknRCCKuIAUi6zh2B1zSp08zMzHpEHbtdzgQukrQcuIl49sss4NsA\nki4G7g8hfCYpfzbwS0mfBK4EBokHrX4gVefXgM9K+i/inZe+ANwP/LDqzpiZmdnWqTz4CCFcnlzT\n4zTirpJfA4tCCI8kRfYgdZRnCGGZpEHgS8njTuAtIYTbUmW+LGkWcB7x0K5/B44MIXT2BhJmZmbW\ncbUccBpCOBc4tyDv8Jy07wHfa1HnqcCpHWiemZmZ1cj3djEzM7NaOfgwMzOzWjn4MDMzs1o5+DAz\nM7NaOfgwMzOzWjn4MDMzs1o5+DAzM7NaOfgwMzOzWlUafEjaWdKlkkYkPS7pAknNbleKpO0kfUPS\nGkl/lPRdSXMzZcYyj1FJR1XZFzMzM+uMqmc+LgPmEW8C9wbgtcRLojfztaTsW5Pyu5N/tdN3Ey/X\nvhvwfOCKzjTZzMzMqlTZ5dUlvRRYBCwIIdySpH0MuFLSiSGEh3KW2RF4H/COEMIvk7T3AislvTqE\ncFOq+Ejq/jBmZmbWJ6qc+VgIPN4IPBJLgQC8pmCZBcSA6JpGQgjhd8C9SX1p35D0iKQbkwDFzMzM\n+kCVN5bbDXg4nRBCGJX0WJJXtMyGEMKTmfTVmWU+B1wLrAVeD5wraYcQwj92pOVmZmZWmdLBh6TT\ngZObFAnE4zwKq0jKlFptepkQwpdSeSskPRv4FNA0+Fi8eDGzZ8/eIm1wcJDBwcGSzTEzM5t6hoaG\nGBoa2iJtZGSk4+tRCOXiAEm7ALu0KPZ74DjgKyGEzWUlDQDPAG8LIfwwp+7DiLtmdk7Pfki6Gzgr\nhHB2QZv+GvgxsH0IYUNO/nxg+fLly5k/f36LppuZmVnD8PAwCxYsgHgM53An6iw98xFCeBR4tFU5\nScuAnSQdmDru4wjiLMaNBYstBzYl5X6Q1PMS4IXAsiarO5B4fMmEwMPMzMx6S2XHfIQQbpe0BDhf\n0oeBbYGvA0ONM10k7U48uPS4EMLNIYQnJX0TOFPS48AfgXOA6xpnukh6IzAXuAFYTzzm49PAl6vq\ni5mZmXVOlQecAhxDPA5jKTAGfBc4IZW/DfASYFYqbTEwmpTdDvgZcHwqfyPwUeAs4izKfwGfCCFc\nUE0XzMzMrJMqDT5CCE8AxzbJvwcYyKStBz6WPPKWWQIs6WAzzczMrEa+t4uZmZnVysGHmZmZ1crB\nh5mZmdXKwYeZmZnVysGHmZmZ1crBh5mZmdXKwYeZmZnVysGHmZmZ1aqy4EPSzpIulTQi6XFJF0ja\nocUyH5D0i2SZMUk7dqJem5zsnQ2tPR638jxmk+NxK89j1huqnPm4DJhHvEncG4DXAue1WGZ74Crg\nS0DR7XYnU69Ngj+kk+NxK89jNjket/I8Zr2hksurS3opsIh4+91bkrSPAVdKOrFxY7msEMI5SdlD\nOlmvmZmZ9Y6qZj4WEm9xf0sqbSlxNuM1PVivmZmZ1aSq4GM34OF0QghhFHgsyeu1es3MzKwmpXa7\nSDodOLlJkUA8HqOwCoqP5dgarep9FsDKlSsrWPXUNTIywvDwcLeb0Xc8buV5zCbH41aex6y81Lbz\nWZ2qUyG0HwtI2gXYpUWx3wPHAV8JIWwuK2kAeAZ4Wwjhhy3WcwhwLbBzCOHJVPp7J1OvpGOAS1u0\n28zMzIq9M4RwWScqKjXzEUJ4FHi0VTlJy4CdJB2YOj7jCOIMxY2lWzlusvUuAd4J3E0MVMzMzKw9\nzwL2Jm5LO6LUzEepiqWfAnOBDwPbAt8CbgohHJfk7w5cAxwXQrg5SduVeOzGQcD/JZ5G+0fg3hDC\n4+3Ua2ZmZr2tyut8HAPcTjwb5SfAr4APpvK3AV4CzEqlfQi4hXjdjgD8EhgG3lSiXjMzM+thlc18\nmJmZmeXxvV3MzMysVg4+zMzMrFZTJviQdLykVZLWSbpB0kEtyr9d0sqk/ApJR9bV1l5RZswkvV/S\nryQ9ljyubjXGU1XZ91pquXckN0z8ftVt7DWT+HzOlvQNSQ8ky9wu6a/qam+vmMS4fSIZq7WS7pV0\npqTt6mpvt0n6C0k/kvSH5LP25jaWOVTScknPSLpD0rvraGuvKDtmkv5G0s8lPZzc4PV6Sa8vu94p\nEXxIOhr4KnAKcCCwAlgiaU5B+YXEG9SdD7wSuAK4QtL+9bS4+8qOGXAIccwOBQ4G7gN+Lun51be2\nd0xi3BrL7QX8A/EA6WllEp/PbYgHlL8Q+FtgP+ADwB9qaXCPmMS4HQOcnpR/KfA+4GjijTqnix2A\nXwPH08YFLSXtTTxx4RrgAOBs4AJJr6uuiT2n1JgRz0L9OXAkMB/4BfBjSQeUWmsIoe8fwA3A2anX\nAu4HTioo/x3gR5m0ZcC53e5Lr45ZzvIzgBHg2G73pdfHLRmrfwfeC1wIfL/b/ejlMSOe9XYnMNDt\ntvfZuH0duDqT9hXgV93uS5fGbwx4c4syZwC/yaQNAT/tdvt7dcwKlvtP4LNllun7mY/kV9ICYuQK\nQIijsZR4I7o8C5P8tCVNyk8pkxyzrB2Ip0s/1vEG9qitGLdTgIdDCBdW28LeM8kxexPJjwFJD0m6\nVdKnJfX991W7Jjlu1wMLGrtmJO0D/DVwZbWt7WsHM423BZ0gScBzKLktKHWF0x41BxgAVmfSVxOn\na/PsVlB+utycbjJjlnUGcRo8+8GdykqPm6Q/J854lJuSnDom817bBzgcuIQ4tfti4Nykni9W08ye\nU3rcQghDyS6Z/0g2CAPAP4UQzqi0pf2taFuwo6TtQgjru9CmfvMp4o/Ry8ssNBWCjyJlb2JX1U3v\n+klbYyDp74CjgENCCBsqb1Xvyx03Sc8G/hn4QEiu0GubNXuvzSBuAP5n8mv/FkkvAE5k+gQfRQrH\nTdKhwGeIu61uAvYFzpH0YAhhuo9bGUqep/v2oKXkOKPPEXfVrCmz7FQIPtYAo8CumfS5TIxoGx4q\nWX6qmcyYASDpROAk4IgQwm+raV7PKjtufwLsRTwYq/GFNgNA0gZgvxDCqora2ism8157ENiQBB4N\nK4HdJM0MIWzqfDN7zmTG7TTg4tTuvd8mAfB5OGgrUrQteNI/rJqT9A7ibVDeFkL4Rdnl+34faghh\nI7CceIM5YPM+qCOI+0DzLEuXT7wuSZ/yJjlmSPoU8L+ARWH8xn7TxiTGbSXwcuIZVQckjx8R79h8\nAPGMoSltku+164i/2tP2Ax6cJoHHZMdtFvGAwbSxZFHllLf8bcHrmSbbgsmSNAh8ExgMIfxsUpV0\n++jaDh2hexSwDngX8RSz84h3331ekn8x8Pep8guBDcAniV9qpxLvdrt/t/vSw2N2UjJGf0P8pdB4\n7NDtvvTyuOUsPx3Pdin7XtuDeCbV2cTjPd5A/IX6d93uS4+P2ynAE8TTa/cm/qC6E7is232pccx2\nIAb2ryQGXp9IXu+Z5J8OXJQqvzfwFPEYtv2AjyTbhr/sdl96eMwGkzH6UGZbsGOp9Xa74x0cwI8A\ndycf1mXAq1J51wLfypR/K/EGdeuA3xB/zXe9H706ZsAq4jRw9vH5bvejl8ctZ9lpF3xMZsyA1xB/\n4a9NNqAnk9yLajo9Sn5GZxD3v98BPJ0sd07ZjUI/P4jXIxrL+Z76VpJ/IXBtzjLLkzG+k3in9a73\npVfHjHhdj7xtQeH3Xt7DN5YzMzOzWvX9MR9mZmbWXxx8mJmZWa0cfJiZmVmtHHyYmZlZrRx8mJmZ\nWa0cfJiZmVmtHHyYmZlZrRx8mJmZWa0cfJiZmVmtHHyYmZlZrRx8mJmZWa0cfJiZmVmtHHyYmZlZ\nrRx8mJmZWa0cfJiZmVmtHHyYmZlZrRx8mJmZWa0cfJiZmVmtHHyYmZlZrRx8mJmZWa0cfJiZmVmt\nHHyYmZlZrRx8mJmZWa0cfJiZmVmtHHyYmZlZrRx8mJmZWa0cfJiZmVmtZna7AXWR9EJgTrfbYWZm\n1ofWhBDu7VRlCiF0qq6eFQOPbe6Bjd1uipmZWT9aC8zrVAAyXWY+5sTA423AXMa7PZA8p18X5aWf\nmy3feN1s+XbLptOzeU0UNSv9XJTX2BGXXl2zvMay2bxsG9L5ee3JLtssr0yZVussW6bxutlYbG5f\nEtgPjCZlxgDQjPh6YGbyPDDGjIGJaQAzGmUZYwZJGpuSVTZeN54bZUcZSNIGkrRmZcbTJpYZX/fE\ntOyyMzavc1OTMkX1lG/fTEZzltvaPjRv3/h4jjbpw/j/p1T7RpO/Nz/H90/yFmBgFBQXJ1kFyeLj\nz2M5eWOZ1+n8ovrSy2zKScurbywnL++5nTJlypapb2vGL2/c8sY0tcymUdiU5G1K0pJ/L5tGxxdp\nLJ6tJpu+qckq06/zlitaplV9m4A1wPdhFnHvgYOP8uYCuwPbJK8b3U+/bpbXeN1OmWZ5rcq2U6aA\nkgeMbyDzNp7ZVWQ3nun8ZnmN52Z52WU7tc52ymTLlmlXs+e21pkEHzNHt3jWQPxYz9gm2ZDNHB0P\nOmYmG67G683BR3pj19i4ZTdg4/nFee2UmVh/O+tur0yrtpdrX7PlJ9eHTrRvIKd9MzJlSMqw+XUj\n2BhItlIzN79m87OabZVg67dkecu0U0+ZdbdTT5l1t9OHMuueTD0z8pfZCGwK43/DeBCyMbVI4+90\nWtHrZnlV1VcFH3BqZmZmtXLwYWZmZrVy8GFmZma1cvBhZmZmtXLwYWZmZrVy8GFmZma1cvBhZmZm\ntXLwYWZmZrVy8GFmZma1cvBhZmZmtXLwYWZmZrVy8GFmZma1cvBhZmZmtXLwYWZmZrVy8NHXhrrd\ngGrcMUX7dcW/dLsFlVg1dFO3m1CJa4bWdLsJHTf0s263oBpDq7rdgmpc1e0GVMjBR1+bohvpKRt8\nXN7tFlTi7ikafFw7FYOPJd1uQTWG7u52C6rh4MPMzMysQxx8mJmZWa0cfJiZmVmtZna7AfV6OHlu\ndHsg53VRXvq52fKN182Wb7dsOj2bBzACDLOFkDwAxpLnUSaWoaBMIxzdlNO8Gam0bDOzedkupfMH\nMmWyZdePwOrh/Lz0MkXL56UXrbNsmcbrZmOxuX3JQA8kg/vkCNx6C2FGfD02Mz5rYGy8TJLGQPzH\njDXKMsZo8s+akfxzxl83nkeTVY8ykKQNJGnNyoynTSzTSM9La5TdMLKOR4fv3byOgaR9efUX11O+\nfTMZzVlucn3IK/P0yCbuGH56i/aNj+dokz5sSto3Vq59o8k6RkPyTCzbeEuMghqf18bnczTzPJaT\nl/qMjzwFw7en8ovqS38vbMpJSz+n87N5ec/tlClTdhRGNsDwY22U3ZrxS6dnx6JgmU2j8QGwKUlL\n/r1bLNL4O1vNU8DKTH7RKpvVV1R/O/VtAqo4+kkhhNal+pykFxL/h7O63RYzM7M+tBaYF0K4txOV\nTYvgAzYHIHO63Q4zM7M+tKZTgQdMo+DDzMzMeoMPODUzM7NaOfgwMzOzWjn4MDMzs1o5+DAzM7Na\nOfgwMzOzWjn4qImk4yWtkrRO0g2SDmpR/u2SViblV0g6MqfMaZIekLRW0tWS9s3k7yzpUkkjkh6X\ndIGkHVL5h0i6IqnjKUnDko7p935lyu4r6Y+SHsvL78d+STpR0u8kPSPpPkmf7uc+SVokaZmkJyU9\nLOm7kvZqp09d7NdnJF0n6emi95akPSVdmZR5SNKXJbX9nduL/ZL0CkmXSbo3qeO3kj7ebp96tV+Z\nss+VdL+kUUk7ToV+SXpPso51yXvx6+32qzIhBD8qfgBHA88A7wJeCpwHPAbMKSi/ENgIfBLYD/jf\nwHpg/1SZk5M63gT8KXAFcBewbarMVcRLoL4K+DPgDuCSVP6nk7oPBl4EfIx4Qbs39HO/UuVmAjcB\nPwEe6/f/V1LmHOA24A3AXsCBwBH92idgb2Ad8IXkPfhK4N+Am3v8f3UKcALwlbz3FvGH3a3AEuDl\nwCLiJZa/2Of9ei/wNeAvkv/dMcDTwEf6uV+Zdf6A+J0xCuzY7/1K1nFf0sYXJXW9sZ1+Vfno6sqn\nywO4ATg79VrA/cBJBeW/A/wok7YMODf1+gFgcer1jsQv8aOS1/OIF/k9MFVmETG42K1JW38CXDAV\n+gWcAVwEvLvVF04/9CspswHYd6q8B4G3Ausz63ljUmagF/uVWTb3vQUcSdy4zEmlfRB4HJjZr/0q\nWPc/Akt79X1Ypl/Ah4FrgcMoF3z0ZL+AnYjB4aHt9KPOh3e7VEzSNsAC4JpGWojviqXE6DfPwiQ/\nbUmjvKR9gN0ydT4J3Jiq82Dg8RDCLak6lhLv7PKaJk2eTYy2m+r1fkk6nLhhO75VX/qoX28k/vJ5\ns6TfJ1O850vauY/7tBwYk/ReSTMkzQaOA64OIWTvStQr/WrHwcCtIYT0bTGWED9fL2u2YI/3K0+v\nf2e0RdL+wGeJ77+xFsXTy/Vyv15HDIT2lHSb4m7af5G0R4k6KuHgo3pziLcZW51JX018c+XZrUX5\nXYlf4M3K7Mb4nfQASL7MHytar6SjiNPj3ypoV1rP9kvSLsCFwLtDCE+10Ze0nu0XsA9xqvttwLHE\nXzsLgH9t0h/o4T6FEO4mzoacTpx2fhzYgzhF3Eq3+tWOovU08prp5X5tQdKfAUcRdzO00rP9krQt\ncBlwYgjhD+0ul+jZfhG/MwaIu9g/TvxB9lzgakldvbGsg4/uEVveX7YT5SddRtJhxKDj/SGE2ycs\n1b5e6Nf5wKUhhOtSeVurF/o1A9gWOC6EcH0I4VfA/wAOl/TiEm0rs/6y5UuVkbQr8f91ITHwfS0x\nCPleiXZNpg1ly5ets5nJ1tNT/ZLUOA7h1BDCNa3Kd7ANVfTr/wC3hRCGUsunnyejF/o1g3js28dC\nCEtDCDcBg8CLibuWusbBR/XWEPcd7ppJn8vEqLbhoRblHyK+CVuVmZvOlDQA7Jxdr6RDgB8CJ4QQ\nLm3Sl7Re7NdDSdJhwImSNkraCFwA7CRpg6T3NO9WT/arUeZBYFMI4a5UsZXJ8wsL2ga93afjgZEQ\nwqdDCCtCCP9BnPY+QtKrm/Spm/1qR956Gq9b1dPL/QI276JYCvxTCOH0Nhfr5X4dBrw99Z2xNKn3\nEUmntFi2l/v1YPLc+J4g2RW4hubfGZVz8FGxEMJG4n7tIxppkpS8vr5gsWXp8onXJemEEFYR35zp\nOnck7ke/PlXHTpIOTNVxBPENfWNquUOJB5meFEL4Zp/366bk9cHEsyYOSB6fB55M/v5BH/ar8f+6\nDpgp6UUwtMWVAAACJ0lEQVSpMvsRfwnd06d9mkX84k5r7G9v+v3UxX61YxnwcknpO2m/Hhghnq1U\nqMf7haSXEQ/KvDCE8Pl2l+vxfv0t498XBwDvJ36u/hvwjWYL9ni/GrO/+6XqeS5xV1Hhd0Ytun3E\n63R4EPeJrmPL07AeBZ6X5F8M/H2q/ELiWQ2N07BOJZ7GlT4N66SkjjcRT+W7AriTLU/D+ilwM3AQ\n8OfA74B/TuUfCjwFfJEYYTceO/dzv3La2faR+73cL+JG+/8BvyAGVwuIX1ZX9XGfDiOe2fI5YF9g\nPvAz4oG12/Vwv/ZkPLAdYXyjtUOSPwNYQTzV+BXE41pWA1/o8fdgq369jHgcz8Vs+Z2Re0ppv/Qr\np52HEIPgds926dl+EX9w/SZZ558CP05etzybrMpH11Y83R7AR4C7kzfoMuBVqbxrgW9lyr8VuD0p\n/xtgUU6dpxJPx1pLPFJ630z+TsAlyZvyceK+9Vmp/AuJvzqzj2v7uV859ZUKPnq5X8SDzf41KfMA\nyS6lPu/TUcQA5Unir70fAC/p8f9V0WfntakyexJnFZ8iBh5nADP6uV/E60rk5f++n/uVU98hlDjV\ntpf7BTyb+Ll7FHiE+P3xgnb7VdVDSePMzMzMauFjPszMzKxWDj7MzMysVg4+zMzMrFYOPszMzKxW\nDj7MzMysVg4+zMzMrFYOPszMzKxWDj7MzMysVg4+zMzMrFYOPszMzKxWDj7MzMysVv8fTtPOn7v0\ni28AAAAASUVORK5CYII=\n",
      "text/plain": [
       "<matplotlib.figure.Figure at 0x7f972cd731d0>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "plt.figure()\n",
    "ax = plot(u, mode=\"displacement\")\n",
    "cbar = plt.colorbar(ax, orientation='horizontal')\n",
    "plt.ylim([-0.1, 0.1])\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1.4303234112999959"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "t"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
