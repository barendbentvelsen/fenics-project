{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Modal analysis and structural stability using FEniCS #\n",
    "\n",
    "<i> Ana Budisa, Barend Bentvelsen </i>\n",
    "\n",
    "This document is the assignment proposal for Modal analysis and structural stability using FEniCS for both static and dynamic problems.\n",
    "\n",
    "The assignment we relate this project to is the Tacoma Narrows Bridge problem, although instead of looking at the fluid-structure interaction we are going to focus on the structural part and analyze the stability of the bridge response without looking in-depth at the loading.\n",
    "\n",
    "### Goal ###\n",
    "The goal of the assignment is to analyze a FE-problem by making use of its eigenvalues and eigenvectors for stability and modal analysis. The eigenvalues and eigenvectors give the structural modes of the system which tell us whether the system is stable and what response we will get in a computationally efficient way. In August we will present our work in a ten-minute presentation and a poster.\n",
    "\n",
    "### Introduction ###\n",
    "In structural engineering modal analysis is a valuable method to find the most important buckling or vibration modes. These modes are used for stability and response analysis. These analyses are of critical importance to any sound structural design since a structure should not lose stability and collapse. The response will tell us the operating envelope and it can be used to improve the design. The technique relies on computing the response of a system using the natural modes of a system using the expansion theorem. The modes themselves are found as eigenvalues of the system.\n",
    "\n",
    "<img src=\"https://upload.wikimedia.org/wikipedia/en/2/2e/Image-Tacoma_Narrows_Bridge1.gif\" \n",
    " width=\"300\" height=\"200\" border=\"10\" title=\"Tacoma Narrows bridge twisting in the wind [Source: Wikipedia]\"/>\n",
    "\n",
    "The system analyzed is based on a mechanical system with mass, stiffness and external forces. However the principles of modal analysis and eigenvalue analysis can also be applied with minor adaptations on other physical problems, which can be described by ODEs. \n",
    "\n",
    "### Discretized equations of motion ###\n",
    "Modes are calculated from the equations of motion which can be written either in the physical domain or in the phase space. Furthermore, modes are generally based on the linear system and damping is not taken into account. The linear system is an approximation which is even valid for nonlinear structures if they are linearized around their equilibrium configuration.\n",
    "\n",
    "The discretized equations of motion of the mechanical system are written in the canonical matrix form using the response vector $\\mathbf{u}$ and its acceleration $\\ddot{\\mathbf{u}}(t) = \\frac{\\partial^2 u(t)}{\\partial t^2}$, mass matrix $\\mathbf{M}$, stiffness matrix  $\\mathbf{K}$ and force vector $\\mathbf{f}(t)$:\n",
    "$$\n",
    "\\mathbf{M}\\ddot{\\mathbf{u}}(t) + \\mathbf{K} \\mathbf{u}(t) = \\mathbf{f}(t)\n",
    "$$\n",
    "\n",
    "The idea is to project the system on a modal basis using a number of $N$ modes to greatly simplify calculations. We do this by using the following ansatz for the solution using mode shape $\\phi_n$ and (potentially complex) frequency $\\omega_n$:\n",
    "$$ u(x,t) = \\sum_{n=1}^{N} c_n \\phi_n e^{\\omega_n t}$$ "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Buckling mode analysis #####\n",
    "Buckling analysis computes the static loads required for a mechanical system to lose (linear) stability. This can sometimes result in quite dramatic changes in stability resulting in collapse of an entire structure. Buckling mode shapes are the modal basis for finding response to a given static load. Buckling mode shapes and critical loads are found by solving the eigenvalue problem:\n",
    "$$\n",
    "\\sum_{n=1}^N \\left(\\mathbf{K} - \\lambda \\mathbf{I} \\right) c_n \\phi_n e^{\\lambda t} = \\mathbf{0}\n",
    "$$\n",
    "\n",
    "#### Vibration mode analysis ####\n",
    "Vibration mode analysis gives the natural resonance frequencies of a system. Resonance can be either good (energy harvesters) or bad (Tacoma Narrows bridge). The vibrational mode shape is a shape in which the system oscillates at it's natural frequency, as well as the modal basis for dynamic calculations. Vibrational mode shapes and natural frequencies are found by solving the quadratic eigenvalue problem:\n",
    "$$\n",
    "\\sum_{n=1}^N \\left(\\mathbf{M} \\omega_n^2 + \\mathbf{K} \\right) c_n \\phi_n e^{\\omega_n t} = \\mathbf{0}\n",
    "$$\n",
    "\n",
    "### Plan of approach ###\n",
    "First of all, we are going to write static and dynamic modal solvers based on the lego brick mesh and verify this with lectures 6 and 7. Once we have working modal solvers, we will analyze the effect of the mesh on modal analysis and convergence rates. Also, we can apply them on a structural model.\n",
    "\n",
    "1. <i> Write static modal solver </i>\n",
    "    1. Write a static modal solver\n",
    "    2. Verify with direct static solver\n",
    "    3. Test what factors influence convergence (number of modes, mesh size, type of mesh)\n",
    "\n",
    "1. <i> Write dynamic modal solver </i>\n",
    "    1. Write a dynamic modal solver\n",
    "    2. Verify with direct time-integration solver\n",
    "    3. Test what factors influence convergence (number of modes, mesh size, type of mesh, time-integration method)\n",
    "    \n",
    "3. <i> Apply modal analysis on a structural model </i>\n",
    "    1. Create structural model\n",
    "    2. Find natural frequencies and mode shapes\n",
    "\n",
    "#### Challenges ####\n",
    "The main challenge here is to successfully implement modal analysis on different examples of structures. \n",
    "\n",
    "Another challenge would be to study the influence of the type of elements in the mesh on the matrices computed in order to understand how it relates to the eigenvalues and eigenvectors obtained.\n",
    "\n",
    "#### Examples and expected outcome ####\n",
    "To write the modal solvers we use a simple model such as a lego block. The problem is easily verified with the work done during the course.\n",
    "\n",
    "The structural model chosen for now is the Tacoma Narrows bridge. The generally accepted theory on why the bridge failed is due to aeroelastic flutter, which is a synchronization of a structural mode to the wake oscillation. The information we will get is the threshold at which the wake oscillations become dangerous."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
