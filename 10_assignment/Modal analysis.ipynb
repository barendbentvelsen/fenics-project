{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Modal analysis #\n",
    "This notebook intends to explain the theory and techniques of modal analysis. The focus is on vibratory modes which are used in dynamic mechanical problems."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Introduction](#Introduction)\n",
    "[Modal projection](#Modal-projection)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction ##\n",
    "Modal analysis is a technique used to reduce models and to find information such as the principal responses, natural frequencies (for dynamic problems) or critical loads (for static problems). The technique relies on computing the response of a system using the natural modes of a system, using the expansion theorem. The modes themselves are found as eigenvalues of the system. \n",
    "\n",
    "The system analyzed is based on a mechanical system with mass, stiffness and external forces. However the principles of modal analysis and eigenvalue analysis can also be applied with minor adaptations on other physical problems which can be described by ODEs."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Equations of motion ###\n",
    "Modes are calculated from the equations of motion which can be written either in the physical domain or in state space, furthermore modes are generally based on the linear system and damping is not taken into account. The linear system is an approximation which is even valid for nonlinear structures, if they are linearized around their equilibrium configuration.\n",
    "\n",
    "Since we are analyzing a linear system, we have constant mass and stiffness, therefore the equations of motion of the mechanical system are written in the canonical matrix form as:\n",
    "$$\n",
    "\\mathbf{M}\\ddot{\\mathbf{x}}(t) + \\mathbf{K} \\mathbf{x}(t) = \\mathbf{f}(t)\n",
    "$$\n",
    "\n",
    "With vectors and matrices being defined over all degrees of freedom:\n",
    "+ Response vector $\\mathbf{x}$ \n",
    "+ Acceleration vector $\\ddot{\\mathbf{x}}(t) = \\frac{\\mathop{d^2} x(t)}{\\mathop{dt^2}}$\n",
    "+ Mass matrix $\\mathbf{M}$\n",
    "+ Stiffness matrix $\\mathbf{K}$\n",
    "+ Force vector $\\mathbf{f}(t)$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This problem can be directly solved with a time-integration method given initial conditions, which are written as: $\\mathbf{x}(0) = \\mathbf{x}_0 $"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Modal projection ##\n",
    "Here modal analysis theory is explained using modal projection. In this section the background of modal analysis is shown using seperation of variables where the (temporal) modal amplitude is seperated from the nodal response vector and this is manipulated until we have expressions for the modal amplitude in both the complex and real domain."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Seperation of variables ###\n",
    "A mode derives from seperation of variables, where the response of the continuous system depends on both position $x$ and time $t$, assuming a solution for the $i^{th}$ mode $x_i$ of the form:\n",
    "\n",
    "$$\n",
    "\\mathbf{x}_i(t) = {X}_i q(t)\n",
    "$$\n",
    "\n",
    "Where $X_i$ is a constant vector and $T$ is a function of time $t$. Substituting this in the equation of motion of a free vibrating system gives:\n",
    "$$\n",
    "\\mathbf{M}{X}_i\\ddot{T}(t) + \\mathbf{K} {X}_i T(t) = \\mathbf{0}\n",
    "$$\n",
    "\n",
    "Which can be rewritten as:\n",
    "$$\n",
    "-\\frac{\\ddot{T}(t)}{T(t)} = \\frac{\\mathbf{K} {X}_i}{\\mathbf{M}{X}_i}  = constant = \\omega^2\n",
    "$$\n",
    "\n",
    "This leads to two seperate problems:\n",
    "$$\n",
    "\\left[ \\mathbf{K} - \\omega^2 \\mathbf{M} \\right]{X}_i = \\mathbf{0} \\\\\n",
    "\\ddot{T}(t) + \\omega^2 T(t) = 0\n",
    "$$\n",
    "\n",
    "The first problem is an eigenpair problem with eigenvector $X_i$ and eigenvalue $\\omega^2$. The temporal problem can be solved in either the real or complex domain which is shown in [temporal solution](#Temporal-solution), where $\\omega$ is shown to be the natural frequency of the mode. So from an engineering point of view we already have found the principal motions and frequencies of the system.\n",
    "\n",
    "Furthermore we see whether the system is stable in case the eigenvalue is complex, where the definition of the modal amplitude decides "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Modal amplitude ###\n",
    "So now we can see that the mode shapes are found from an eigenvalue problem and the time-function can be described as a harmonic with a phase and amplitude which are determined by initial conditions. \n",
    "\n",
    "#### Real domain ####\n",
    "We can write our solution with one harmonic where the unknowns are phase and amplitude or using two harmonics where the unknowns are the two amplitudes:\n",
    "$$\n",
    "T(t) = C_i \\cos \\left( \\omega t + \\phi_i \\right) = C_i^{(c)} \\cos \\left( \\omega t \\right) + C_i^{(s)} \\sin \\left( \\omega t \\right) \n",
    "$$\n",
    "\n",
    "#### Complex domain ####\n",
    "Or in complex notation where there are two unknowns in the solution as well:\n",
    "$$\n",
    "T(t)  = \\frac{C_i}{2}\\left( e^{-i(\\omega t + \\phi_i)} + e^{i(\\omega t + \\phi_i)}\\right) = K_{i}^{(n)} e^{-i\\omega t} + K_{i}^{(p)} e^{i\\omega t}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Stability ###\n",
    "All these solutions for $T$ are equivalent, the constants are found by the initial conditions. The temporal solutions all satisfy $ \\ddot{T}_i = -\\omega^2_i T_i $.\n",
    "\n",
    "Sometimes the solution in the complex domain is noted by $e^{s_n t}$ with $s_n = \\alpha + i\\beta$ so that in the above case $\\beta = \\omega$ and $\\alpha$ is the growth rate (if positive) or the decay rate (if negative). There are three cases to be considered:\n",
    "+ stable, $\\alpha < 0$, this leads to $$\\lim_{t\\to \\infty} e^{\\left(\\alpha + i\\beta\\right) t} = 0$$ so the response dies out eventually\n",
    "+ unstable, $\\alpha > 0$, this leads to $$\\lim_{t\\to \\infty} e^{\\left(\\alpha + i\\beta\\right) t} = \\infty $$ so the response explodes\n",
    "+ neutrally stable, $\\alpha = 0$, this is usually the undamped solution where the same modal amplitude continues  infinity. $$\\lim_{t\\to \\infty} e^{\\left(\\alpha + i\\beta\\right) t} = \\lim_{t\\to \\infty} e^{i\\beta t} $$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Modal matrices ###\n",
    "The modal equation changes to:\n",
    "\n",
    "$$\n",
    "-\\omega^2_i \\mathbf{M}{X}_i {T}(t) + \\mathbf{K} {X}_i T(t) = \\mathbf{0}\n",
    "$$\n",
    "\n",
    "Therefore the temporal part can be taken out of the equation, then multiplying by ${X}_j^T$ gives:\n",
    "$$\n",
    "\\omega_i^2 {X}_j^T \\mathbf{M}{X}_i = {X}_j^T \\mathbf{K} {X}_i \n",
    "$$\n",
    "By considering the symmetry of the mass and stiffness matrices:\n",
    "$$\n",
    "{X}_j^T \\mathbf{M}{X}_i = {X}_i^T \\mathbf{M}{X}_j \\\\\n",
    "{X}_j^T \\mathbf{K}{X}_i = {X}_i^T \\mathbf{K}{X}_j \n",
    "$$\n",
    "We can see the $j^{th}$ mode multiplied by $X_i^T$ can be written as:\n",
    "$$\n",
    "\\omega_j^2 {X}_i^T \\mathbf{M}{X}_j = {X}_i^T \\mathbf{K} {X}_j \n",
    "$$\n",
    "\n",
    "Subtracting the $j^{th}$ mode from the $i^{th}$ mode gives:\n",
    "$$\n",
    "{X}_j^T \\mathbf{K}{X}_i - {X}_i^T \\mathbf{K}{X}_j = 0 \\\\\n",
    "(\\omega_i^2 - \\omega_j^2) {X}_j^T \\mathbf{M}{X}_i = 0\n",
    "$$\n",
    "\n",
    "From the above relation it becomes clear that in general when the natural frequencies do not match when $i\\neq j$ and when the frequencies do match the mass is normalized so the stiffness is equal to the natural frequency squared:\n",
    "$$\n",
    "\\begin{align}\n",
    "\\omega_i \\neq \\omega_j  & {}  & {} & \\omega_i = \\omega_j \\\\\n",
    "{X}_j^T \\mathbf{K}{X}_i = 0 & & & {X}_i^T \\mathbf{K}{X}_i = K_{ii} = \\omega_i^2 \\\\\n",
    "{X}_j^T \\mathbf{M}{X}_i = 0 & & & {X}_i^T \\mathbf{M}{X}_i = M_{ii} = 1\n",
    "\\end{align}\n",
    "$$\n",
    "\n",
    "\n",
    "So that when using the modal matrix $\\mathbf{X} = \\left[X_1, X_2, \\dots, X_n \\right]$ and the modes are properly normalized this results in a natural frequency matrix and an identity matrix:\n",
    "$$\n",
    "\\mathbf{X}^T \\mathbf{M}\\mathbf{X} = \\mathbf{M}_M = \\mathbf{I} \\\\\n",
    "\\mathbf{X}^T \\mathbf{K}\\mathbf{X} = \\mathbf{K}_M =  \\begin{bmatrix} \\omega_1^2 & & & \\\\\n",
    "& \\omega_2^2 & & \\\\\n",
    "& & \\ddots & \\\\\n",
    "& & & \\omega_n^2\n",
    "\\end{bmatrix} = \\pmb{\\omega}^2 \n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Expansion theorem ###\n",
    "The $n$ eigenvectors $\\mathbf{X}_i$ are linearly independent, thus they form a basis in the $n$-dimensional space. The expansion theorem states that we can solve the response vector of size $n$ a linear combinations of $n$ linearly  independent vectors. This can be expressed as:\n",
    "\n",
    "$$\n",
    "\\mathbf{x} = \\sum_{i=1}^n c_i \\mathbf{X}^{(i)}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Free vibration ###"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With free vibration the force applied is zero, this leads to the equations of motion in the form (using complex modes in the form $x_n = c_{i} \\mathbf{X}^{(i)} e^{i \\omega t})$:\n",
    "$$\n",
    "\\sum_{i=1}^n c_{i}  \\left(- \\omega_i^2 \\mathbf{M} + \\mathbf{K} \\right)\\mathbf{X}^{(i)} e^{i \\omega_i t} = \\mathbf{0}\n",
    "$$\n",
    "\n",
    "We can premultiply this equation with $\\sum_{j=1}^n X^{(j)}$ which because of orthogonality results in modal mass and stiffness matrices which are the unity and frequency matrix respectively:\n",
    "$$\n",
    "c_{i} \\left( \\mathbf{I} - \\pmb{\\omega}^2 \\right)\\mathbf{X}^{(i)} e^{i \\omega_i t} = \\mathbf{0}\n",
    "$$\n",
    "\n",
    "By definition: $\\left( \\mathbf{I} - \\pmb{\\omega}^2 \\right)\\mathbf{X}^{(i)} = \\mathbf{X}^{(i)T} \\left( -\\omega_i^2\\mathbf{M} + \\mathbf{K} \\right)\\mathbf{X}^{(i)} = \\mathbf{0}$. \n",
    "\n",
    "The modal coefficient $c_i$ is free, but when introducing initial conditions the variable is fixed. If we look at the expansion theorem:\n",
    "$$\n",
    "\\mathbf{x}(t = t_0) = \\sum_{i=1}^n c_i \\mathbf{X}^{(i)}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Forced vibration ###"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Again using complex modes ($x_n = c_{i} \\mathbf{X}^{(i)} e^{\\omega t})$) the problem becomes::\n",
    "$$\n",
    "\\sum_{i=1}^n c_{i}  \\left(\\omega_i^2 \\mathbf{M} + \\mathbf{K} \\right)\\mathbf{X}^{(i)} e^{\\omega_i t} = \\mathbf{f}(t)\n",
    "$$\n",
    "\n",
    "We can premultiply this equation again with $\\sum_{j=1}^n X^{(j) T}$ which leads to:\n",
    "$$\n",
    "c_{i} \\left( \\mathbf{I} + \\pmb{\\omega}^2 \\right)\\mathbf{X}^{(i)} e^{\\omega_i t} = X^{(i) T} \\mathbf{f}(t)\n",
    "$$\n",
    "\n",
    "The modal coefficient $c_i$ is now fixed by forces applied on this specific mode which corresponds to the modal force $f_m(t) = X^{(i) T} \\cdot \\mathbf{f}(t)$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Computing modes ##"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Physical space ###"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### State space ###"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
